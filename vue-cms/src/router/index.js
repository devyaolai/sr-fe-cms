import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/pages/index/index'
import Native from '@/pages/native/native'
import Banner from '@/pages/banner/banner'
import Preview from '@/pages/preview/preview'
import Relative from '@/pages/relative/relative'
import Profile from '@/pages/profile/profile'
import Promotion from '@/pages/promotion/promotion'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/upload',
      name: 'Index',
      meta: {title: '图片上传'},
      component: Index
    },
    {
      path: '/banner',
      name: 'Banner',
      meta: {title: 'banner'},
      component: Banner
    },
    {
      path: '/native',
      name: 'Native',
      meta: {title: '客户端'},
      component: Native
    },
    {
      path: '/preview',
      name: 'Preview',
      meta: {title: '预览'},
      component: Preview
    },
    {
      path: '/relative',
      name: 'Relative',
      meta: {title: '相关地址'},
      component: Relative
    },
    {
      path: '/profile',
      name: 'Profile',
      meta: {title: '权益中心后台管理'},
      component: Profile
    },
    {
      path: '/promotion',
      name: 'Promotion',
      meta: {title: '餐饮后台管理'},
      component: Promotion
    },
    {
      path: '/',
      redirect: 'relative'
    }
  ]
})
